
install

go get gitlab.com/silo-city-labs-llc/golang/go-wireless/

use

import iwlib "gitlab.com/silo-city-labs-llc/golang/go-wireless/"

iwlib
------

Requires iwlib installed

Example:
``` go
	var iface iwlib.NetworkInterface = "wlan0"

	//Scan for new networks
	results, err := iface.WirelessScan()
	if err != nil {
		log.Fatal("Error:", err)
	}

	var firstSsid := results[0].Config.Essid


	//Current connection
	c, err := iface.BasicConfig()
	if err != nil {
		log.Fatal("Error:", err)
	}

	var currentSsid := c.Essid

```

wpactl
------

Requires a running wpa_supplicant with control interface at `/var/run/wpa_supplicant`.

Example:
``` go
wpa_ctl, err := NewController("wlan0")
if err != nil {
	log.Fatal("Error:", err)
}
defer wpa_ctl.Cleanup()

err = wpa_ctl.ReloadConfiguration()
if err != nil {
	log.Fatal("Error:", err)
}

networks, err := wpa_ctl.ListNetworks()
if err != nil {
	log.Fatal("Error retrieving networks:", err)
}
for _,network := range networks {
	log.Println("NET", network)
}

i, _ := wpa_ctl.AddNetwork()
wpa_ctl.SetNetworkSettingString(i, "ssid", "Wifi Name")
wpa_ctl.SetNetworkSettingString(i, "psk", "some password")
wpa_ctl.SetNetworkSettingRaw(i, "scan_ssid", "1")
wpa_ctl.SetNetworkSettingRaw(i, "key_mgmt", "WPA-PSK")
wpa_ctl.SelectNetwork(i)
wpa_ctl.SaveConfiguration()

for {
	event := <- wpa_ctl.EventChannel
	switch event.Name {
		case "CTRL-EVENT-DISCONNECTED":
			log.Println("Disconnected")
		case "CTRL-EVENT-CONNECTED":
			log.Println("Connected")
		case "CTRL-EVENT-SSID-TEMP-DISABLED":
			log.Println("InvalidKey")
	}
}
```